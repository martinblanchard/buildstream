Install
=======
This section covers how to install BuildStream onto your machine, how to run
BuildStream inside a docker image and also how to configure an artifact server.

.. note::

   BuildStream is not currently supported natively on macOS and Windows. Windows
   and macOS users should refer to :ref:`docker`.

.. toctree::
   :maxdepth: 2

   install_linux_distro
   install_docker
   install_artifacts
